#!/usr/bin/env python3

"""
e-liquid math toolbox.

A DIY e-liquid receipt is a combination of:

- A glycol base which may contain nicotine or not
- A nicotine booster
- A flavor

This program gives the required quantities depending on each ingredient
concentration and limiting ingredient volume.
"""

import argparse
from functools import reduce

__version__ = "0.0.1"

inf = float('inf')
epsilon = 0.000000001

# TODO allow base concentration > target concentration by adding a 0mg base
# TODO allow multiflavor receipts

# Parse arguments, see --help option for more information
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

concentration = parser.add_argument_group(title='Concentration')
concentration.add_argument(
    '-n', '--nicotine', type=float,
    help='target nicotine concentration in mg/ml',
    default=3)
concentration.add_argument(
    '-F', '--flavor-concentration', dest='fc', type=float,
    help='flavor concentration in %%',
    default=15)
concentration.add_argument(
    '-B', '--booster-concentration', dest='bc', type=float,
    help='booster nicotine concentration in mg/ml',
    default=20)
concentration.add_argument(
    '-G', '--glycol-concentration', dest='gc', type=float,
    help='base nicotine concentration in mg/ml',
    default=0)

volume = parser.add_argument_group(title='maximum volume available')
volume.add_argument(
    '-t', '--total', type=float, default=inf,
    help='total volume in ml')
volume.add_argument(
    '-f', '--flavor-max', dest='fm', type=float, default=inf,
    help='max flavor volume in ml')
volume.add_argument(
    '-b', '--booster-max', dest='bm', type=float, default=inf,
    help='max booster volume in ml')
volume.add_argument(
    '-g', '--glycol-max', dest='gm', type=float, default=inf,
    help='max base volume in ml')

misc = parser.add_argument_group(title='Miscellaneous')
misc.add_argument(
    '-v', '--version',
    action='version',
    version='%(prog)s {version}'.format(version=__version__))
misc.add_argument(
    '-d', "--debug",
    help='debug mode, verbose output',
    action='store_true')

args = parser.parse_args()

if args.fm == inf and args.bm == inf and args.gm == inf and args.total == inf:
    parser.error("""
        at least one of [flavor-max, booster-max, glycol-max, total]
        is required""")

# Find volume independent coefficients
f_coeff = args.fc / 100 or epsilon
t2_coeff = 1 - f_coeff
g_coeff = (args.nicotine - t2_coeff * args.bc) / (args.gc - args.bc)  or epsilon
b_coeff = t2_coeff - g_coeff  or epsilon

# Find limiting product
limiting_volume = reduce(min, [v[0] / v[1] for v in [
    (args.fm, f_coeff),
    (args.bm, b_coeff),
    (args.gm, g_coeff),
    (args.total, 1),
]])

print("base   : %5.1f ml" % (g_coeff * limiting_volume))
print("booster: %5.1f ml" % (b_coeff * limiting_volume))
print("flavor : %5.1f ml" % (f_coeff * limiting_volume))
print("total  : %5.1f ml" % ((g_coeff + b_coeff + f_coeff) * limiting_volume))
