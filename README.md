# liquid

compute e-liquid components volumes given their proportions and a maximum volume.

See ```liquid -h``` for help.

## Install

- copy liquid.py (or symlink it without the .py extension) to a folder in $PATH

## Example

- Find coefficients to create a receipt with 4 mg/ml of nicotine and a total volume of 50 ml. The flavor default proportion is 15 %, the glycol default nicotine concentration is 0% and the nicotine booster defaut concentration is 20 mg/ml :

```liquid.py -n 4 -t 50```

This outputs:

```
base   :  32.5 ml
booster:  10.0 ml
flavor :   7.5 ml
```
